package com.narayone.denistiufanov.moneykeeper;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.narayone.denistiufanov.moneykeeper.database.OperationType;
import com.narayone.denistiufanov.moneykeeper.database.OperationsDataBaseHelper;

import java.util.ArrayList;
import java.util.HashSet;

public class AddOperationDialog extends Dialog {

    @NonNull
    private Activity activity;
    @NonNull
    private final OperationType type;
    @NonNull
    private final OperationsDataBaseHelper mDatabaseHelper;

    private Spinner spinnerOperation;

    private EditText inputOperationName;

    private String operationName;

    public AddOperationDialog(@NonNull final Activity activity,
                              @NonNull final OperationType type,
                              @NonNull final OperationsDataBaseHelper mDatabaseHelper) {
        super(activity);
        this.activity = activity;
        this.type = type;
        this.mDatabaseHelper = mDatabaseHelper;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.operation_dialog);

        spinnerOperation = (Spinner) findViewById(R.id.inputVariants);
        inputOperationName = (EditText) findViewById(R.id.inputOperationName);
        loadSpinnerDataHama();
        spinnerOperation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                operationName = parent.getItemAtPosition(position).toString();
                if (operationName.equalsIgnoreCase("NEW")) {
                    inputOperationName.setVisibility(View.VISIBLE);
                } else {
                    inputOperationName.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Button addButton = (Button) findViewById(R.id.addOperation);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SQLiteDatabase sdb;
                float value = 0;
                sdb = mDatabaseHelper.getReadableDatabase();
                EditText valueFiled = (EditText) findViewById(R.id.inputSize);
                if (valueFiled != null) {
                    String textValue = valueFiled.getText().toString();
                    if (!textValue.isEmpty()) {
                        value = Float.valueOf(textValue);
                    }
                }
                if (value > 0) {
                    if (inputOperationName.getVisibility() == View.VISIBLE
                            && inputOperationName.getText().length() > 0) {
                        operationName = inputOperationName.getText().toString();
                    }
                    if (operationName.equalsIgnoreCase("NEW")) {
                        return;
                    }
                    ContentValues values = new ContentValues();
                    values.put(OperationsDataBaseHelper.TYPE_COLUMN, type.name());
                    values.put(OperationsDataBaseHelper.OPERATION_NAME_COLUMN, operationName);
                    values.put(OperationsDataBaseHelper.SUM_COLUMN, value);
                    values.put(OperationsDataBaseHelper.DATA_COLUMN, System.currentTimeMillis());
                    sdb.insert(OperationsDataBaseHelper.DATABASE_TABLE, null, values);
                }
                dismiss();
            }
        });
    }

    public HashSet<String> getNeededLabels() {
        HashSet<String> labels = new HashSet<String>();
        // Select All Query
        String selectQuery = "SELECT " + "*" + " FROM " + OperationsDataBaseHelper.DATABASE_TABLE;

        SQLiteDatabase db = new OperationsDataBaseHelper(getContext()).getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                if (type.name().equalsIgnoreCase(cursor
                        .getString(cursor
                                .getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)))) {
                    labels.add(cursor
                            .getString(cursor
                                    .getColumnIndex(OperationsDataBaseHelper.OPERATION_NAME_COLUMN)));
                }
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning labels
        return labels;
    }

    private void loadSpinnerDataHama() {
        // Spinner Drop down elements
        HashSet<String> lables = getNeededLabels();
        ArrayList<String> listObjects = new ArrayList<>();
        listObjects.add("NEW");
        listObjects.addAll(lables);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, listObjects);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerOperation.setAdapter(dataAdapter);
    }
}
