package com.narayone.denistiufanov.moneykeeper;

import android.view.View;

public class LinearExampleAdapter extends RecyclerBindableAdapter<Integer, LinearViewHolder> {

    private LinearViewHolder.ActionListener actionListener;

    //задаем layout id для нашего элемента
    @Override
    protected int layoutId(int type) {
        return R.layout.linear_example_item;
    }

    //Создаем ViewHolder
    @Override
    protected LinearViewHolder viewHolder(View view, int type) {
        return new LinearViewHolder(view);
    }

    //Изменяем данные внутри элемента
    @Override
    protected void onBindItemViewHolder(LinearViewHolder viewHolder, final int position, int type) {
        viewHolder.bindView(getItem(position), position, actionListener);
    }

    //интерфейс для обработки событий
    public void setActionListener(LinearViewHolder.ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    @Override
    public void addHeader(View header) {
        if (getHeadersCount() == 0) {
            super.addHeader(header);
        } else {
            removeHeader(getHeader(0));
            super.addHeader(header);
        }
    }

    @Override
    public void addFooter(View footer) {
        if (getFootersCount() == 0) {
            super.addFooter(footer);
        } else {
            removeFooter(getFooter(0));
            super.addFooter(footer);
        }
    }
}
