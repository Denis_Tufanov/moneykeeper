package com.narayone.denistiufanov.moneykeeper;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.narayone.denistiufanov.moneykeeper.database.OperationType;
import com.narayone.denistiufanov.moneykeeper.database.OperationsDataBaseHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity{
    private OperationsDataBaseHelper mDatabaseHelper;
    private LinearExampleAdapter operationsInStrings;
    @NonNull
    private ArrayList<OperationObject> listObjects = new ArrayList<>();
    private TextView leftView;
    private TextView spentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        leftView = (TextView) findViewById(R.id.sum_of_left);
        spentView = (TextView) findViewById(R.id.sum_of_spent);
        operationsInStrings = new LinearExampleAdapter();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(operationsInStrings);
        }
        mDatabaseHelper = new OperationsDataBaseHelper(this);
        refreshScreen();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void insertDataInDatabase(@NonNull final OperationType type) {
        AddOperationDialog operationDialog = new AddOperationDialog(this, type, mDatabaseHelper);
        operationDialog.show();
        operationDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                refreshScreen();
            }
        });
    }

    void refreshScreen() {
        final SQLiteDatabase sdb;
        sdb = mDatabaseHelper.getReadableDatabase();
        Cursor cursor = sdb.query(OperationsDataBaseHelper.DATABASE_TABLE,
                new String[]{OperationsDataBaseHelper.TYPE_COLUMN,
                        OperationsDataBaseHelper.OPERATION_NAME_COLUMN,
                        OperationsDataBaseHelper.SUM_COLUMN,
                        OperationsDataBaseHelper.DATA_COLUMN},
                null, null,
                null, null, null);
        listObjects.clear();
        float spentSum = 0;
        float addedSum = 0;
        if (!cursor.isClosed()) {
            while (cursor.moveToNext()) {
                OperationObject operationObject = new OperationObject();
                operationObject.operationType = cursor
                        .getString(cursor.getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN));
                operationObject.operationName = cursor
                        .getString(cursor.getColumnIndex(OperationsDataBaseHelper.OPERATION_NAME_COLUMN));
                float sum = cursor
                        .getFloat(cursor.getColumnIndex(OperationsDataBaseHelper.SUM_COLUMN));
                long timeOfAction = cursor
                        .getLong(cursor.getColumnIndex(OperationsDataBaseHelper.DATA_COLUMN));
                operationObject.sum = String.valueOf(sum);
                operationObject.timeOfAction = DateFormat.format("dd/MM/yyyy", new Date(timeOfAction)).toString();
                listObjects.add(operationObject);
                if (operationObject.operationType.equalsIgnoreCase(OperationType.INCOME.name())) {
                    addedSum += sum;
                } else if (operationObject.operationType.equalsIgnoreCase(OperationType.OUTCOME.name())) {
                    spentSum += sum;
                }
            }
            leftView.setText(String.format(Locale.US, "%.2f", (addedSum - spentSum)));
            spentView.setText(String.format(Locale.US, "%.2f", (spentSum)));
//            operationsInStrings.notifyDataSetChanged();
            cursor.close();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP){
            insertDataInDatabase(OperationType.INCOME);
            refreshScreen();
        }else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
            insertDataInDatabase(OperationType.OUTCOME);
            refreshScreen();
        }else {
            return super.onKeyDown(keyCode, event);
        }
        return true;
    }
}
