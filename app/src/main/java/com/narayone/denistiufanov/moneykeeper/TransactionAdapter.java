package com.narayone.denistiufanov.moneykeeper;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.narayone.denistiufanov.moneykeeper.database.OperationType;
import com.narayone.denistiufanov.moneykeeper.database.OperationsDataBaseHelper;

import java.util.ArrayList;

public class TransactionAdapter extends ArrayAdapter<String> {
    private static final String TAG = "TransactionAdapter";
    private final Context context;
    private final ArrayList<OperationObject> values;
    private final int width;

    public TransactionAdapter(Context context, ArrayList values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.main_view_element, parent, false);
        rowView.setOnTouchListener(new View.OnTouchListener() {
            float curentPosition;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        curentPosition = event.getX();
                        return true;
                    case MotionEvent.ACTION_UP:
                        float difference = curentPosition - event.getX();
                        if (Math.abs(difference) > (width / 2)) {
                            deleteEvent(position);
                        }
                        return true;
                }
                return false;
            }
        });
        TextView operationNameView = (TextView) rowView.findViewById(R.id.operationNameView);
        TextView operationSumView = (TextView) rowView.findViewById(R.id.operationSumView);
        TextView operationTimeView = (TextView) rowView.findViewById(R.id.operationTimeView);
        ImageView operationTypeView = (ImageView) rowView.findViewById(R.id.imageView);

        OperationObject operationObject = values.get(position);
        operationNameView.setText(operationObject.operationName);
        operationSumView.setText(operationObject.sum);
        operationTimeView.setText(operationObject.timeOfAction);
        String uri = null;
        Drawable res = null;
        if (operationObject.operationType.equalsIgnoreCase(OperationType.INCOME.name())) {
            res = context.getResources().getDrawable(R.drawable.plus);
        } else if (operationObject.operationType.equalsIgnoreCase(OperationType.OUTCOME.name())) {
            res = context.getResources().getDrawable(R.drawable.minus);
        } else {
            res = context.getResources().getDrawable(R.drawable.minus);
        }
        operationTypeView.setImageDrawable(res);
        return rowView;
    }

    private void deleteEvent(int pointerCount) {
        Log.i(TAG, "deleteEvent: " + String.valueOf(pointerCount));
        OperationsDataBaseHelper.deleteDBElement(context, values.get(pointerCount));
        values.remove(pointerCount);
        notifyDataSetInvalidated();
    }
}

