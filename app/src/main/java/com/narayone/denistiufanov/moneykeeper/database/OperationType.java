package com.narayone.denistiufanov.moneykeeper.database;

public enum OperationType {
    INCOME,
    OUTCOME
}
