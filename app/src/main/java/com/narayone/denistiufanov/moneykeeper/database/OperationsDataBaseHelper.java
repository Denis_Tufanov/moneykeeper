package com.narayone.denistiufanov.moneykeeper.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.narayone.denistiufanov.moneykeeper.OperationObject;

import java.util.HashSet;

public class OperationsDataBaseHelper extends SQLiteOpenHelper implements BaseColumns {

    // имя базы данных
    private static final String DATABASE_NAME = "moneykeeperdatabase.db";
    // версия базы данных
    private static final int DATABASE_VERSION = 1;
    // имя таблицы
    public static final String DATABASE_TABLE = "operations";
    // названия столбцов
    public static final String TYPE_COLUMN = "operation_type";
    public static final String OPERATION_NAME_COLUMN = "operation_name";
    public static final String SUM_COLUMN = "sum";
    public static final String DATA_COLUMN = "data";
    // создание таблицы
    private static final String DATABASE_CREATE_SCRIPT = "create table "
            + DATABASE_TABLE
            + " (" + BaseColumns._ID + " integer primary key autoincrement, "
            + TYPE_COLUMN + " enum, "
            + OPERATION_NAME_COLUMN + " text not null, "
            + SUM_COLUMN + " float, "
            + DATA_COLUMN + " long);";

    public OperationsDataBaseHelper(Context context,
                                    SQLiteDatabase.CursorFactory factory,
                                    DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION, errorHandler);
    }

    public OperationsDataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Запишем в журнал
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);

        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
        // Создаём новую таблицу
        onCreate(db);
    }

    public static void deleteDBElement(Context context, OperationObject object) {
        OperationsDataBaseHelper mDatabaseHelper = new OperationsDataBaseHelper(context);
        final SQLiteDatabase sdb = mDatabaseHelper.getWritableDatabase();

        StringBuilder whereClause = new StringBuilder();
        whereClause.append(OperationsDataBaseHelper.TYPE_COLUMN + "=?" + " and ");
        whereClause.append(OperationsDataBaseHelper.OPERATION_NAME_COLUMN + "=?" + " and ");
        whereClause.append(OperationsDataBaseHelper.SUM_COLUMN + "=?" + " and ");
        whereClause.append(OperationsDataBaseHelper.DATA_COLUMN + "=?");
        String[] whereArgs = new String[]{
                object.operationType,
                object.operationName,
                object.sum,
                object.timeOfAction};
        sdb.beginTransaction();
        sdb.delete(OperationsDataBaseHelper.DATABASE_TABLE, whereClause.toString(), whereArgs);
        sdb.endTransaction();
        checkIfRawDeleted(context, object);

        sdb.close();
    }

    private static boolean checkIfRawDeleted(Context context, OperationObject object) {
        HashSet<String> labels = new HashSet<String>();
        // Select All Query
        String selectQuery = "SELECT " + "*" + " FROM " + OperationsDataBaseHelper.DATABASE_TABLE;

        SQLiteDatabase db = new OperationsDataBaseHelper(context).getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        boolean detected = false;
        if (cursor.moveToFirst()) {
            do {
                Log.d("raw " + cursor.getCount(),
                        TYPE_COLUMN + "=" + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)) + ", "
                                + OPERATION_NAME_COLUMN + "=" + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.OPERATION_NAME_COLUMN)) + ", "
                                + SUM_COLUMN + "=" + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.SUM_COLUMN)) + ", "
                                + DATA_COLUMN + "=" + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.DATA_COLUMN)) + ", ");
                if (object.operationType.equalsIgnoreCase(cursor
                        .getString(cursor.getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)))
                        && object.operationName.equalsIgnoreCase(cursor
                        .getString(cursor.getColumnIndex(OperationsDataBaseHelper.OPERATION_NAME_COLUMN)))
                        && object.sum.equalsIgnoreCase(cursor
                        .getString(cursor.getColumnIndex(OperationsDataBaseHelper.SUM_COLUMN)))
                        && object.timeOfAction.equalsIgnoreCase(cursor.getString(cursor
                        .getColumnIndex(OperationsDataBaseHelper.DATA_COLUMN)))) {
                    Log.d("SQLDataBase", "detected raw deleting");
                    detected = true;
                }
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        return detected;
    }

    public static void insertDBElement(Context context, OperationObject object) {

    }
}
